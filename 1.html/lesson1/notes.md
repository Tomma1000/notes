
# Basis html

## What is html?

HyperText Markup Language

### What is it for?

Its a way structure data.

## Building blocks of html

### Anatomy:

![html tag](htmltag.gif)

```
<p class="default-style">paragraph</p>
```

* openingtag
`<p>`
* closing tag
`</p>`
* element content
`paragraph`
* element
`<p>paragraph<p>`
* attributes
`class="default-style"`

### Other

* self closing elements
`<img src="" alt="">`
* comments
`<!-- -->`

## HTML document

```
<!DOCTYPE html>
<html>
  <head>
    <title>Document</title>
  </head>
  <body>
  </body>
</html>
```

## HTML Elements

### Doctype
html5 doctype:
`<!DOCTYPE html>`

[The Common DOCTYPES - CSS Tricks](https://css-tricks.com/snippets/html/the-common-doctypes/)

### Base elements
* `<html></html>`, `<head></head>`, `<body></body>`,...

[htmlreference.io](http://htmlreference.io/base/)

### Meta vs. non-meta

* meta tags/elements
* html tags/elements

### Block vs. inline

* block: `<div></div>`, `<p></p>`,...
* inline: `<span></span>`, `<b></b>`, `<em></em>` 
	* img
	* a
	* br
	* em
	* strong

### Common elements

* heading 1: `<h1></h1>`
* heading 2: `<h2></h2>`
* heading 2: `<h6></h6>`
* unordered list: 
* ordered list
* list item:
* img:
* a:
```
<a href=""></a>
```
	* link to element inside page
	* link to external page
* tables and table elements

	```
	<table>
		<thead>
			<tr>
				<th></th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td></td>
			</tr>
		</tbody>
	</table>
	```
* hr
* br

### New HTML5 elements

* header
* footer
* nav
* main
* section
* article
* aside

## Page load

* Web inspector - Elements Tab, Network Tab

* DOM - Document Object Model
[What is the DOM? - CSS Tricks](https://css-tricks.com/dom/)

## Websites to use:

* HTML Validator
  http://validator.w3.org
  This websites validates your code

* HTML Reference
  http://www.htmlreference.io
  An overview of the most common HTML elements with more info on the element attributes

* CSS Reference
  http://www.cssreference.io

* MDN or Modzilla Development Network
  https://developer.mozilla.org/en-US/

## CSS

### What is CSS

Cascading Style Sheet

### How to add CSS to web page

* inline: on html

	`<p style="color: green;">`
	
* embedded: between style tags in the head section of a page

	```
	<style>
		p {
			color: green;
		}
	</style>
	```

* linked: in an external file linked to the page 

	`<link rel="stylesheet" href="link/to/stylesheet.css">`

* @import inside a external stylesheet document

### How can CSS style HTML?

With CSS rules

Anatomy of a CSS rule:
![css rule](cssrule.png)

#### Different selectors
* ID selectors 
* Class selectors 
* Attribute selectors
* Pseudo selectors

[How CSS Selectors Work - CSS Tricks](https://css-tricks.com/how-css-selectors-work/)

#### Properties

[cssreference.io](http://cssreference.io/)

[CSS Properties - MDN](https://developer.mozilla.org/en-US/docs/Web/CSS/Reference)


### Web page

### How is page displayed in the browser

1. Downloaded over HTTP - HyperText Transfer Protocol
2. 


## Extra

### Lorem Ipsum

- [Lorem Ipsum](http://www.lipsum.com/)

- [Other placeholder text](http://idsgn.dropmark.com/107)
