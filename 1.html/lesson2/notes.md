## Shortcuts

### In the browser

- CTRL + I = opens the inspector / developer tools
- CTRL + U = Show page Source

### In Visual Studio Code

* Toggle line comment

	`CTRL + /`

* Brings the cursor to the next line, even if you are in the middle of the scentence

	`CTRL + ENTER`

* Moves current line up

	`CTRL + Arrow Up`

* Moves current line down

	`CTRL + Arrow Down`

* Select exact same peace of code or text
  1. select something
  2. CTRL + d = select next exact same

* Open intellisence

	`CTRL + space`

* Write a html faster with emmet

	`write '.wrapper'` + `tab`
	
	or
	
	`write '#header'` + `tab`

## Code editor settings

Example:

`"editor.wordWrap": "on",`


## CSS

### Comments

```
p {
	/*color: red; */
}
```

### Selector names

* first character
	* underscore: _
	* hyphen: -
	* letter: a–z
	
* next characters
	* hyphens
	* underscores
	* letters
	* numbers
	
There is a catch: if the first character is a hyphen, the second character must2 be a letter or underscore, and the name must be at least 2 characters long.
	
[CSS Grammar - W3C](https://www.w3.org/TR/CSS21/grammar.html#scanner)

### Comma seperated selectors

```
h1,
p {
	color: green;
}
```

### Shorthands

* Example 1: 

```
p {
  font-style: italic;
  font-weight: bold;
  font-size: 10px;
  font-family: sans-serif;
}
```
	
is equal to 
	
```
p {
  font: italic bold 10px sans-serif;
}
```

* Example 2

```
div {
	padding-top: 15px;
	padding-right: 15px;
	padding-bottom: 15px;
	padding-left: 15px;
}
```

is equal to 

```
div {
	padding: 15px 15px 15px 15px;
}
```

is equal to 

```
div {
	padding: 15px 15px;
}
```

is equal to 

```
div {
	padding: 15px;
}
```

### Universal selector

```
* {
	border: 1px solid red;
}
```
targets all elements of the page

[MDN Universal Selector - MDN](https://developer.mozilla.org/en-US/docs/Web/CSS/Universal_selectors)

### Units

* colors
	* name
	* hexedecimal
	* rgb - red green blue
	* hsl - heu saturation lightness
	
* dimenstions
	* px
	* em
	* rem
	* ...

* percentage
* reserved words

	examples: 
	
	solid dotted (for `border-style`)
	
* calc();

### Pseudo classes

```
selector:pseudo-class{
  property:value;
}
```

```
a:link  {
  color: red;
} 

a:visited {
  color: green;
} 

a:hover {
  color: blue;
} 

a:focus {
  color: blue;
}

a:active {
  color: yellow;
} 
```

[demo - Codepen](https://codepen.io/pvde/pen/boMMaJ)

### Specificity - cascading priority

#### Specificity basics

* __inline vs embedded vs linked stylesheet__

Styles defined inline will win over embedded or linked styles.
Embedded styles are as specific as linked styles but look at the place they are called.

* __position__

Rules lower in the file overwrite rules higher in the file.
	
```
p {
	color: red;
}
	
p {
	color: green;
}
```

➡️ the color of the paragraph will be green
	
* __selector position__
	
```
div p {
	color: red;
}
	
p {
	color: green;
}
```

➡️ the color of the paragraph will be red
	
* selector types; id's vs classes

`<p class="i-am-a-class" id="i-am-an-id">paragraph</p>`
	
```
.i-am-a-class {
	color: red;
}
#i-am-an-id {
	color: green;
}
```

➡️ the color of the paragraph will be green

* __!important__
	
⚠️ don't use this

```
p {
	color: pink !important;
}
```

#### Calculating CSS Specificity Value

![specificity](specificity.png)

#### More info

[Specifics on CSS Specificity - CSS Tricks](https://css-tricks.com/specifics-on-css-specificity/)

[A Specificity Battle! - CSS Tricks](https://css-tricks.com/a-specificity-battle/)

### Display - visibility

`display: inline|block|inline-block|flex;` vs `display: none;`

`visibility: hidden` vs `visibility: visible`

### Opacity

- percentage

```
p {
	opacity: 0.5;
}
```

⚠️ all child elements will inhirit opacity

### Box model

![box model](boxmodel.png)

* display: inline vs block vs inline-block
	
	example: p vs span
	
	example: space between inline-block
	

* margin
	* center element with margin auto
* border
* padding
* box-sizing
	* border-box
	* content-box

* wrapper

Wrappers are a good way to center content if the screen width is wider than your content.

```
.wrapper{
  width: 100%;
  max-width: 1400px;
  margin: 0 auto;
}
```

### Positioning

```
div {
	position: absolute;
}
```
* static: HTML elements are positioned static by default
* relative
* absolute
* fixed

### Floats & clear

* left 
* right

### Overflow

### Z-index

### Flexbox

[A Complete Guide to Flexbox - CSS Tricks](https://css-tricks.com/snippets/css/a-guide-to-flexbox/)

[Flexbox Froggy](http://flexboxfroggy.com/)

### Browser prefixes

```
.example {
  -ms-word-break: break-all;
      word-break: break-all;

  /* Non standard for webkit */
       word-break: break-word;

  -webkit-hyphens: auto; /* Saf 5.1+ */
     -moz-hyphens: auto; /* Fx 6.0+ */
      -ms-hyphens: auto; /* IE 10+ */
          hyphens: auto; /* None yet */
}
```

[Browser Prefixes](http://shouldiprefix.com/)

Will automate this later.

### Fonts

```
p {
	font-family: "Arial", sans-serif;
}
```

## Extra

- [Placeholder images](https://placeholder.com/)

- [Codepen](https://codepen.io/)

- Dev docs browser extentions

- Editor extentions

example: vscode icons

- [Google Fonts](https://fonts.google.com/)
